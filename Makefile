GENERATED=fdb_latexmk fls log aux out tex toc tex~ html~
ALL_GENERATED=pdf fdb_latexmk fls log aux out tex toc html tex~ html~
.PHONY: clean all tarball
%.tex : %.org
	emacs -Q $< --batch -f org-latex-export-to-latex --kill

%.html: %.org
	emacs -Q $< --batch -f org-html-export-to-html --kill

.DEFAULT_GOAL = all
all: rv-notes.pdf rv-notes.html

clean:
	rm -f $(addprefix rv-notes., ${GENERATED})

distclean:
	rm -f $(addprefix rv-notes., ${ALL_GENERATED})

rv-notes.pdf: rv-notes.tex
	latexmk -pdf $<

tarball:
	git archive HEAD --format tgz --prefix automation-`git rev-parse HEAD`/ -o automation-`git rev-parse --short HEAD`.tgz

